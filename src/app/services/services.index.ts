export { DocenteService } from './docente.service';

export { ActasService } from './actas.service';

export { EstudianteService } from './estudiante.service';

export { DiplomadoService } from "./diplomado.service";

