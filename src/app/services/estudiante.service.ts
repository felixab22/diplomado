import { Injectable } from '@angular/core';
import { EstudianteModel } from '@models/diplomado/diplomado.modal';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URL_SERVICIOS } from '@components/config/config';

@Injectable({
    providedIn: 'root'
})
export class EstudianteService {
    api = URL_SERVICIOS;

    constructor(private http: HttpClient) { }

    // =========================================
    // Estudiantes


    // Para listar los Estudiantes que pertenecen a un diplomado
    public getListaAllEstudent(): Observable<EstudianteModel[]> {
        return this.http.get<EstudianteModel[]>(this.api + `/estudiante/getAllEstudiante`);
    }
    // Para Buscar un Estudiantes por el DNI
    public getEstudentxDNI(dni): Observable<any[]> {
        return this.http.get<any[]>(this.api + `/estudiante/findEstudianteByDni?dni=${dni}`);
    }

    // Para listar los Estudiantes que pertenecen a un diplomado
    public getListaAllDiplomadoxEstudent(id): Observable<any[]> {
        return this.http.get<any[]>(this.api + `/acta/getAllActaByDiplomadosEstudiante?idestudiante=${id}`);
    }

    // /acta/getAllActaByEstudiante?idestudiante= &idsemestre=

    // Para listar los Estudiantes que pertenecen a un diplomado
    public getListaAllDiplomadodeUnEstudent(diestudiate, iddiplomado): Observable<any[]> {
        return this.http.get<any[]>(this.api + `/acta/getAllActaByEstudiante?idestudiante=${diestudiate}&idsemestre=${iddiplomado}`);
    }


    // guardar y editar un Estudiantes
    public saveOrUpdateEstudiante(estudiante: EstudianteModel): Observable<EstudianteModel> {
        return this.http.post<EstudianteModel>(this.api + '/estudiante/saveOrUpdateEstudiante', JSON.stringify(estudiante));
    }
}
