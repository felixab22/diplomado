import { Injectable } from '@angular/core';
import { DiplomadoModal, ModulosModel, SemestreModal } from '@models/diplomado/diplomado.modal';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URL_SERVICIOS } from '@components/config/config';

@Injectable({
  providedIn: 'root'
})
export class DiplomadoService {
  api = URL_SERVICIOS;

  public listas = [
    { id: 1, tipo: 'PENDIENTE' },
    { id: 2, tipo: 'INICIADO' },
    { id: 3, tipo: 'FINALIZADO' }
  ];

  public clases = [
    {
      id_apertura: 1,
      mencion: 'Atención farnaceutica y farmacia clinica',
      ciclo: '2/2018-ii',
      pagina: '001',
      fecha: '23-12-2018',
      estado: 'PENDIENTE',
      id_diplo: {
        id_diplo: 1,
        section: 'Ciencias de la salud',
        asignatura: 'Farmacoterapia i',
        sicle: 'cf-720',
        creditos: 2.0
      }
    }
  ];
  public diplomados = [
    {
      id_diplo: 1,
      section: 'Ciencias de la salud',
      maestria: 'Atención farnaceutica y farmacia clinica',
      mencion: 'Atención farnaceutica y farmacia clinica',
      asignatura: 'Farmacoterapia i',
      sicle: 'cf-720',
      creditos: 2.0
    }
  ];

  constructor(private http: HttpClient) { }

  // Diplomado
  //Listar Diplomado
  public getListaDiplomado(): Observable<DiplomadoModal[]> {
    return this.http.get<DiplomadoModal[]>(this.api + '/getAllDiplomado');
  }
  //Listar Diplomado
  public getDiplomadoxId(id: any): Observable<DiplomadoModal[]> {
    return this.http.get<DiplomadoModal[]>(this.api + `/getDiplomadoById?iddiplomado=${id}`);
  }
  // Eliminar Diplomado
  public deleteDiplomado(id: any): Observable<any> {
    return this.http.post(this.api + '/deleteDiplomado', JSON.stringify(id));
  }
  // Guardar y Editar un diplomado
  public saveOrUpdateDiplomateInit(diploma: DiplomadoModal): Observable<DiplomadoModal> {
    return this.http.post<DiplomadoModal>(this.api + '/saveOrUpdateDiplomado', JSON.stringify(diploma));
  }
  // =========================================
  // Modulos
  // Listar Modulos
  public getListaModulos(): Observable<ModulosModel[]> {
    return this.http.get<ModulosModel[]>(this.api + '/modulo/getAllModulo');
  }
  // Para listar los modulos que pertenecen a un diplomado
  public getListaModulosxIdDiplomado(id: any): Observable<ModulosModel[]> {
    return this.http.get<ModulosModel[]>(this.api + `/modulo/getAllModuloByDiplomado?iddiplomado=${id}`);
  }
  // guardar y editar un diplomado
  public saveOrUpdateModulos(modulo: ModulosModel): Observable<ModulosModel> {
    return this.http.post<ModulosModel>(this.api + '/modulo/saveOrUpdateModulo', JSON.stringify(modulo));
  }



  // ============================================

  // Semestre
  // Listar Semestre
  public getListaSemestre(): Observable<SemestreModal[]> {
    return this.http.get<SemestreModal[]>(this.api + '/semestre/getAllSemestre');
  }


  // guardar y editar un Semestre
  public saveOrUpdateSemestre(modulo: SemestreModal): Observable<SemestreModal> {
    return this.http.post<SemestreModal>(this.api + '/semestre/saveOrUpdateSemestre', JSON.stringify(modulo));
  }
}
