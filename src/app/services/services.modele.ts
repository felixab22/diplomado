import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    DiplomadoService,
    EstudianteService,
    ActasService,
    DocenteService
} from './services.index';




@NgModule({
    imports: [
        CommonModule,
        HttpClientModule
    ],
    providers: [
        DiplomadoService,
        EstudianteService,
        ActasService,
        DocenteService

    ],
    declarations: []
})
export class ServiceModule { }
