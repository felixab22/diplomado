import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URL_SERVICIOS } from '@components/config/config';
import { DocenteModel } from '../models/diplomado/diplomado.modal';

@Injectable({
    providedIn: 'root'
})
export class DocenteService {
    api = URL_SERVICIOS;

    constructor(private http: HttpClient) { }

    // =========================================
    // Actas
    // Listar Actas
    public getListaDocenteAll(): Observable<DocenteModel[]> {
        return this.http.get<DocenteModel[]>(this.api + '/docente/listAllDocente');
    }

    // guardar y editar un Actas
    public saveOrUpdateDocente(docente: DocenteModel): Observable<DocenteModel> {
        return this.http.post<DocenteModel>(this.api + '/docente/saveOrUpdateDocente', JSON.stringify(docente));
    }

}