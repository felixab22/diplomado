import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @Output() Cambiarmenu: EventEmitter<any> = new EventEmitter;
  contador = 1;
  menu1 = true;
  constructor(
    private _router: Router
  ) {

  }

  ngOnInit() {
    this.Cambiarmenu.emit(this.menu1);
  }
  cerrarsesion() {
    this._router.navigate(['login']);
  }
  // escondermenu() {
  //   if (this.contador === 1) {
  //     this.menu1 = true;
  //     this.contador = 0;
  //   } else {
  //     this.menu1 = false;
  //     this.contador = 1;
  //   }
  //   this.Cambiarmenu.emit(this.menu1);
  // }
}
