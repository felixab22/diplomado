import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nopagecomponent',
  templateUrl: './nopagecomponent.component.html',
  styleUrls: ['./nopagecomponent.component.scss']
})
export class NopageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
