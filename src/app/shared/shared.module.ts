import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RouterModule } from '@angular/router';
import { NopageComponent } from './nopagecomponent/nopagecomponent.component';
import { MenuComponent } from './menu/menu.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarModule } from 'ng-sidebar';
@NgModule({
    imports: [
        SidebarModule.forRoot(),
        RouterModule,
        CommonModule,
        MDBBootstrapModule.forRoot(),
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
    ],
    declarations: [
        NopageComponent,
        MenuComponent,
        NavbarComponent
    ],
    exports: [
        NopageComponent,
        MenuComponent,
        NavbarComponent
    ],
    schemas: [NO_ERRORS_SCHEMA],
})
export class SharedModule { }