import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Output() Cambiomenu: EventEmitter<any> = new EventEmitter();
  private _opened: boolean = false;



  constructor(
    private _router: Router
  ) { }

  ngOnInit() {
  }
  cerrarsesion() {
    localStorage.clear();
    this._router.navigate(['login']);
  }
  _toggleSidebar() {
    this._opened = !this._opened;
    this.Cambiomenu.emit(this._opened);
  }
}
