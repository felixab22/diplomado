export class DiplomadoModal {
    iddiplomado: number;
    denomination: string;
}
export class SemestreModal {
    idsemestre: number;
    finiciodiplomado: Date;
    ffindiplomado: Date;
    estado: string;
    diplomado: string;
    ciclo: string;
    iddocente: number;
}
export class IdDiplomadoModel {
    iddiplomado: number;
}
export class ModulosModel {
    nummodulo: number;
    idmodulo: number;
    denominacion: string;
    credfpresencial: number;
    credfnopresencial: number;
    horafpresencial: number;
    horafnopresencial: number;
    diplomado: number;
}
export class EstudianteModel {
    idestudiante: number;
    dni: number;
    nombre: string;
    apellido: string;
    telefono: number;
    codigo: number;
}
export class ActaModel {
    idacta: number;
    fregistro: Date;
    estudiante: number;
    modulo: number;
    semestre: number;
    notamodulo: number;
}
export class DocenteModel {
    iddocente: number;
    nombredocente: number;
    apellidodocete: number;
    dnidocente: number;
    telefono: number;
}
