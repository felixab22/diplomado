import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.scss']
})
export class PdfComponent implements OnInit {
  imprimir: any;
  detallediploma: any;
  fechahoy: string;
  anio: string;
  mes: string;
  dia: string;
  numeration: Array<any>;
  j = 1;
  p = 1;
  constructor() {

    this.numeration = new Array({ numero: '001' });
    this.fechahoy = new Date().toISOString().substr(0, 10);
    this.anio = new Date().toISOString().substr(0, 4);
    this.mes = new Date().toISOString().substr(5, 2);
    this.dia = new Date().toISOString().substr(8, 2);
  }

  ngOnInit() {
    this.imprimir = JSON.parse(localStorage.getItem('imprimir'));
    console.log(this.imprimir);

    this.incrustar();
    this.detallediploma = JSON.parse(localStorage.getItem('cursonotas'));
    // console.log(this.imprimir.length);
  }
  incrustar() {
    for (let item of this.imprimir) {
      if (this.j < 10) {
        item.numero = '00' + this.j.toString();
      } else {
        item.numero = '0' + this.j.toString();
      }
      this.j = this.j + 1;
    }
  }




  createPDF() {
    var sTable = document.getElementById('imprimirpfg').innerHTML;
    var style = "<style>";
    style = style + "div.container {font-family: Arial; width: 210mm; height: 245mm;}";
    style = style + ".header {display: grid; grid-template-columns: 15% 75%;}";
    style = style + "h3 , h5  {text-align: center; padding:0; margin: 0; }";
    style = style + "h6 {font-size: 10px; padding:5px; margin: 5px; }";
    style = style + ".sub-cabeza {display: grid; grid-template-columns: 20% 50% 10% 20%; padding:0;margin: 0; }";
    style = style + ".card {margin-top: 20px;}";
    style = style + ".logo img { width: 50%; height: 100%;}";
    style = style + "table {width: 100%;font: 11px Arial;}";
    style = style + "table, th, td {border: solid 1px black; border-collapse: collapse;}";
    style = style + "th {text-align: center; font-size: 12px;font-weight: 700; font-family: Arial;}";
    style = style + "td {text-align: center; font-size: 12px;font-weight: 400; font-family: Arial;height: 25px;}";
    style = style + "td.nombre {text-align: left;}";
    style = style + ".azul {color: blue;}";
    style = style + ".rojo {color: red;}";
    style = style + ".abajo { font-weight: 600;display: grid; grid-template-rows: 50% 50%;font-family: Arial; height: 55mm;}";
    style = style + ".fecha {font-size: 12px; display: grid; grid-template-columns: repeat(3, 1fr);grid-template-areas: 'a b c d e f'; margin: 40px;}";
    style = style + ".meses {grid-area: f; float: right!important; align-self: end;}";
    style = style + ".define {display: grid; grid-template-columns: 33% 33% 33%; text-align: center; font-size: 12px; font-family: Arial;}";
    // style = style + "padding: 2px 3px;text-align: center;}";

    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>ACTA DIPLOMADO</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }

}
