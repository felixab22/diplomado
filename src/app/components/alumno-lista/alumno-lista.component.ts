import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { EstudianteModel } from '../../models/diplomado/diplomado.modal';
import { EstudianteService } from '@services/services.index';
import { Router } from '@angular/router';
declare var swal: any;

@Component({
  selector: 'app-alumno-lista',
  templateUrl: './alumno-lista.component.html',
  styleUrls: ['./alumno-lista.component.scss']
})
export class AlumnoListaComponent implements OnInit {
  checkNum = true;
  term = '';
  pagina2: number = 1;
  Estudiante: EstudianteModel;
  alumnos: EstudianteModel[];
  listaDiplo: any[];
  idalumno: any;

  constructor(
    @Inject(DOCUMENT) private _document,
    private _EstudianteSrv: EstudianteService,
    private _router: Router,
  ) {
    this.Estudiante = new EstudianteModel();
  }

  ngOnInit() {
    this.Listar();

  }
  masterToggle() {
    const items: any = document.getElementsByName('acs');
    if (this.checkNum === true) {
      for (var i = 0; i < items.length; i++) {
        if (items[i].type == 'checkbox')
          items[i].checked = true;
      }
      this.checkNum = false;
    } else {
      for (var i = 0; i < items.length; i++) {
        if (items[i].type == 'checkbox')
          items[i].checked = false;
      }
      this.checkNum = true;
    }

  }
  Editar(estudiante, basicModal) {
    this.Estudiante = estudiante;
    basicModal.show();
  }
  Listar() {
    this._EstudianteSrv.getListaAllEstudent().subscribe(res => {
      this.alumnos = res;
    });
  }
  GuardarOeditar(basicModal) {
    this._EstudianteSrv.saveOrUpdateEstudiante(this.Estudiante).subscribe((res: any) => {
      console.log(res);
      if (res.code === 200) {
        basicModal.hide();
        swal('Bien!', 'Guardado!', 'success').then(() => {
          this.Listar();
        });
      } else {
        swal('Mal!', 'Los campos con * son obligatorios', 'warning');
      }
    });
  }
  abrirModal12(id, ventana) {
    this.idalumno = id;
    ventana.show();
    this._EstudianteSrv.getListaAllDiplomadoxEstudent(id).subscribe(res => {
      console.log(res);
      this.listaDiplo = res;
    });
  }

  enviarDatos(basicModal12, idsemestre) {
    basicModal12.hide();
    localStorage.setItem('klumno', this.idalumno);
    localStorage.setItem('kidsemestre', idsemestre);
    this._router.navigate(['/Posgrado/Alumnopdf']);
  }
}
