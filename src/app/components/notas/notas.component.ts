import { Component, OnInit, Inject } from '@angular/core';
import { ActasService, DiplomadoService } from '@services/services.index';
import { SemestreModal, ModulosModel, ActaModel } from '@models/diplomado/diplomado.modal';
import { DOCUMENT } from '@angular/platform-browser';
import { Router } from '@angular/router';
declare var swal: any;
@Component({
  selector: 'app-notas',
  templateUrl: './notas.component.html',
  styleUrls: ['./notas.component.scss']
})
export class NotasComponent implements OnInit {
  semestre: SemestreModal;
  iddiplo: any;
  modulos: ModulosModel[];
  alumnonotas: any[];
  newActa: ActaModel;
  buena = 400;
  cambioLista = true;
  pagina3 = 1;
  constructor(
    @Inject(DOCUMENT) private _document,
    private _ActasSrv: ActasService,
    private _DiplomadoSrv: DiplomadoService,
    private _router: Router,
  ) {

  }

  ngOnInit() {
    this.semestre = JSON.parse(localStorage.getItem('cursonotas'));
    this.buscarDiplomado();
    this.buscarModuloxDiplomado();
  }
  buscarDiplomado() {
    const listaDiploma = JSON.parse(localStorage.getItem('listaDiplomados'));

    for (let diploma of listaDiploma) {
      if (diploma.denomination === this.semestre.diplomado) {
        this.iddiplo = diploma.iddiplomado;
      }
    }

  }

  buscarModuloxDiplomado() {
    this._DiplomadoSrv.getListaModulosxIdDiplomado(this.iddiplo).subscribe(res => {
      this.modulos = res;

    });
  }
  setNewListar(modulo) {
    this._ActasSrv.getSemestrexModulo(this.semestre.idsemestre, modulo).subscribe((res: any) => {
      this.alumnonotas = res.data;
      this.cambioLista = false;
    });
  }
  colorear(link, acta: any) {
    let selectores: any = this._document.getElementsByClassName('selector');
    for (let ref of selectores) {
      ref.classList.remove('colorVerde');
    }
    link.classList.add('colorVerde');
    this.newActa = new ActaModel();
    this.newActa.idacta = acta.idacta;
    this.newActa.modulo = acta.modulo.idmodulo;
    this.newActa.fregistro = acta.fregistro;
    this.newActa.semestre = acta.semestre.idsemestre;
    this.newActa.estudiante = acta.estudiante.idestudiante;
    // this.newActa. = acta.estudiante.idestudiante;
  }

  editarNota(editarNota) {
    let selectores: any = this._document.getElementsByClassName('chekeado');
    if (editarNota < 0 || editarNota > 20) {
      swal('Mal!', 'LA NOTA DEBE SER ENTRE 0 Y 20', 'warning');
      return;
    } else {
      this.newActa.notamodulo = editarNota;
      this.SaveOrUpdateActas(this.newActa);
    }
  }
  SaveOrUpdateActas(acta) {
    this._ActasSrv.saveOrUpdateActa(acta).subscribe((res: any) => {
      console.log(res);
      if (res.code === 200) {
        this.buena = 200;
      } else {
        console.log('error');
        this.buena = 400;
      }
    });
  }

  MostrarEditar(alumnos) {
    localStorage.setItem('imprimir', JSON.stringify(alumnos));
    this._router.navigate(['/Posgrado/pdf']);
  }
  IrALista() {
    this._router.navigate(['/Posgrado/Semestre-alumno/', this.semestre.idsemestre]);
  }

}
