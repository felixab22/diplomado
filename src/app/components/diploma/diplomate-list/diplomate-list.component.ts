import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { DiplomadoModal, IdDiplomadoModel } from '@models/diplomado/diplomado.modal';
import { DiplomadoService } from '@services/services.index';
import { Router } from '@angular/router';

declare var swal: any;

@Component({
  selector: 'app-diplomate-list',
  templateUrl: './diplomate-list.component.html',
  styleUrls: ['./diplomate-list.component.scss']
})
export class DiplomateListComponent implements OnInit {
  selecteparticipaAct: Array<IdDiplomadoModel>;
  newDiplomado: DiplomadoModal;
  checkNum = true;
  diplomados: Array<DiplomadoModal>;
  term = '';
  p = 1;
  idsdiploamtura: Array<any>;

  constructor(
    @Inject(DOCUMENT) private _document,
    private _DiplomateSrv: DiplomadoService,
    private _router: Router,

  ) {
    this.newDiplomado = new DiplomadoModal();
    this.idsdiploamtura = new Array();
    this.selecteparticipaAct = new Array<IdDiplomadoModel>();
  }

  ngOnInit() {
    this.listadelStorage();
  }

  listadelStorage() {
    this.diplomados = JSON.parse(localStorage.getItem('listaDiplomados'));
    for (let diploma of this.diplomados) {
      this.idsdiploamtura.push(diploma.iddiplomado);
    }
  }

  imprimirDiplomado() {
    this._DiplomateSrv.getListaDiplomado().subscribe(res => {
      this.diplomados = res;
      localStorage.setItem('listaDiplomados', JSON.stringify(this.diplomados));
      for (let diploma of this.diplomados) {
        this.idsdiploamtura.push(diploma.iddiplomado);
      }
    });
  }
  //guardar o editar la diplomatura
  GuardarOeditar(basicModal) {
    this._DiplomateSrv.saveOrUpdateDiplomateInit(this.newDiplomado).subscribe((res: any) => {
      if (res.code === 200) {
        swal('Bien!', 'Guardado!', 'success').then(() => {
          this.imprimirDiplomado();
          basicModal.hide();
        });
      } else {
        swal('Mal!', 'Los campos con * son obligatorios', 'warning');
      }
    });
  }
  Editar(EditDiploma, basicModal) {
    this.newDiplomado = EditDiploma;
    basicModal.show();
  }
  masterToggle() {
    const items: any = document.getElementsByName('acs');
    if (this.checkNum === true) {
      for (var i = 0; i < items.length; i++) {
        if (items[i].type == 'checkbox')
          items[i].checked = true;
      }
      this.checkNum = false;
    } else {
      for (var i = 0; i < items.length; i++) {
        if (items[i].type == 'checkbox')
          items[i].checked = false;
      }
      this.checkNum = true;
    }
  }
  eventCheckbox(event, contactId) {
    if (event.srcElement.checked) {
      let localId = new IdDiplomadoModel();
      localId.iddiplomado = contactId.iddiplomado;
      this.selecteparticipaAct.push(localId);
    } else {
      const elimanarcontac = this.selecteparticipaAct.findIndex(element => element.iddiplomado === contactId.iddiplomado);
      this.selecteparticipaAct.splice(elimanarcontac, 1);
    }
  }
  iniciarNuevo(basicModal4) {
    this.newDiplomado = new DiplomadoModal();
    basicModal4.show();
  }
  eliminar() {
    console.log(this.selecteparticipaAct);
    swal({
      title: "Desea eliminar?",
      text: "Esta seguro de eliminar!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this._DiplomateSrv.deleteDiplomado(this.selecteparticipaAct).subscribe(res => {
            this.imprimirDiplomado();
          })
          swal("Eliminado!", {
            icon: "success",
          });
        } else {
          swal("Cancelado!");
        }
      });
  }
  iniciarModulo(diploma) {
    localStorage.setItem('diploma', JSON.stringify(diploma));
    this._router.navigate(['/Posgrado/Modulos', diploma.iddiplomado]);
  }

}
