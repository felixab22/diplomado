import { Component, OnInit } from '@angular/core';
import { DiplomadoService, DocenteService } from '@services/services.index';
import { DiplomadoModal, SemestreModal, DocenteModel } from '@models/diplomado/diplomado.modal';
import { Router } from '@angular/router';
declare var swal: any;
@Component({
  selector: 'app-apertura-diploma',
  templateUrl: './apertura-diploma.component.html',
  styleUrls: ['./apertura-diploma.component.scss']
})
export class AperturaDiplomaComponent implements OnInit {
  clases: any;

  listas: { id: number; tipo: string; }[];
  listafilter: any;
  listacategoria: any;
  hoy: any;
  maniana: any;
  anio1: string;
  mes1: string;
  anio2: number;
  mes2: number;
  primero: string;
  segundo: string;
  diplomados: Array<DiplomadoModal>;
  semestre: SemestreModal;
  activo: boolean;
  docentes: DocenteModel[];
  constructor(
    private _DocenteSrv: DocenteService,
    private _DiplomadoSvr: DiplomadoService,
    private _Router: Router,
  ) {
    this.semestre = new SemestreModal();
    this.listafilter = this.listacategoria = this.clases;
    // fecha de hoy
    this.anio1 = new Date().toISOString().substr(0, 4);
    this.mes1 = new Date().toISOString().substr(5, 2);
    if (parseInt(this.mes1) === 12) {
      this.anio2 = parseInt(this.anio1) + 1;
      this.mes2 = 1;

      this.primero = this.anio1 + '-' + this.mes1 + '-02';
      this.segundo = this.anio2 + '-0' + this.mes2 + '-02';
    } else {
      this.anio2 = parseInt(this.anio1);
      this.mes2 = parseInt(this.mes1) + 1;
      if (parseInt(this.mes1) < 10) {
        this.primero = this.anio1 + '-' + this.mes1 + '-02';
        this.segundo = this.anio2 + '-0' + this.mes2 + '-02';
      } else {
        this.primero = this.anio1 + '-' + this.mes1 + '-02';
        this.segundo = this.anio2 + '-' + this.mes2 + '-02';
      }
    }

    this.hoy = this.primero;
    this.maniana = this.segundo;
  }

  ngOnInit() {
    this.imprimir();
    this.listadelStorageDiplomado();
    this.listarDocente();
    this.activo = true;
  }


  imprimir() {
    this.listas = this._DiplomadoSvr.listas;
    this._DiplomadoSvr.getListaSemestre().subscribe(res => {
      this.listafilter = this.listacategoria = this.clases = res;
      console.log(res);
    });
  }
  listadelStorageDiplomado() {
    this.diplomados = JSON.parse(localStorage.getItem('listaDiplomados'));
  }

  setNewListar(tipo) {

    if (tipo === 'ALL') {
      this.listacategoria = this.clases;
      this.listafilter = this.clases;
    }
    else {
      this.listacategoria = this.clases.filter((course) => {
        return course.estado === tipo;
      });

      this.listafilter = [...this.listacategoria];

    }
  }

  setNewListarDocente(docente) {
    console.log(docente);
    this.semestre.iddocente = parseInt(docente);
    this.activo = false;

  }

  // estados
  // PENDIENTE
  // INICIO
  // FINALIZADO

  setNewListardiplomados(diplomado) {
    console.log(diplomado);
    this.semestre.estado = 'PENDIENTE';
    this.semestre.diplomado = diplomado;

  }
  NuevoApertura(basicModal3) {
    basicModal3.show();
  }
  filtroFechaInicio(evento): void {
    console.log('inicio', evento);



  }
  filtroFechaFin(evento) {
    console.log('fin', evento);
  }
  GuardaryActualizar(basicModal3) {
    // this.semestre.ciclo = this.anio1;
    this._DiplomadoSvr.saveOrUpdateSemestre(this.semestre).subscribe((res: any) => {
      if (res.code = 200) {
        swal('Bien!', 'Guardado!', 'success').then(() => {
          basicModal3.hide();
          this.activo = true;
          this.imprimir();
          this.semestre = new SemestreModal();
        });
      } else {
        swal('Mal!', 'Los campos con * son obligatorios', 'warning');
      }
    });
  }
  IniciarRegistro(course) {
    localStorage.setItem('curso', JSON.stringify(course));
    this._Router.navigate(['/Posgrado/Actas']);
  }
  RegistrarNotas(curso) {
    localStorage.setItem('cursonotas', JSON.stringify(curso));
    this._Router.navigate(['/Posgrado/Notas']);
  }
  listarDocente() {
    this._DocenteSrv.getListaDocenteAll().subscribe(res => {
      this.docentes = res;
    });
  }
}
