import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ComponentsComponent } from './components.component';
import { BienvenidoComponent } from './config/bienvenido/bienvenido.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccountSettingsComponent } from './config/account-settings/account-settings.component';
import { ChartsComponent } from './charts/charts.component';
import { ChartComponent } from './charts/chart/chart.component';
import { SidebarModule } from 'ng-sidebar';
import { DiplomateListComponent } from './diploma/diplomate-list/diplomate-list.component';
import { AperturaDiplomaComponent } from './diploma/apertura-diploma/apertura-diploma.component';
import { AlumnoListaComponent } from './alumno-lista/alumno-lista.component';
import { ServiceModule } from '@services/services.modele';
import { ModulosComponent } from './modulos/modulos.component';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { ActasComponent } from './actas/actas.component';
import { NotasComponent } from './notas/notas.component'; // <-- import the module
import { NumeroPipe } from '@pipes/numero.pipe';
import { PdfComponent } from './pdf/pdf.component';
import { DocentesComponent } from './docentes/docentes.component';
import { MesesPipe } from '@pipes/mes.pipe';
import { SemestreAlumnoComponent } from './semestre-alumno/semestre-alumno.component';
import { AlumnoPdfComponent } from './alumno-pdf/alumno-pdf.component';
import { RomanosPipe } from '@pipes/romanos.pipe';
@NgModule({
    declarations: [
        NumeroPipe,
        RomanosPipe,
        MesesPipe,
        ComponentsComponent,
        BienvenidoComponent,
        AccountSettingsComponent,
        ChartsComponent,
        ChartComponent,
        DiplomateListComponent,
        AperturaDiplomaComponent,
        AlumnoListaComponent,
        ModulosComponent,
        ActasComponent,
        NotasComponent,
        PdfComponent,
        DocentesComponent,
        SemestreAlumnoComponent,
        AlumnoPdfComponent
    ],
    exports: [
        BienvenidoComponent
    ],

    imports: [
        Ng2SearchPipeModule,
        NgxPaginationModule,
        SidebarModule.forRoot(),
        BrowserModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        SharedModule,
        MDBBootstrapModule.forRoot(),
        ServiceModule

    ],
    providers: [],
    schemas: [NO_ERRORS_SCHEMA],

})
export class ComponentsModule { }