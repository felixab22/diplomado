import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-components',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.scss']
})
export class ComponentsComponent implements OnInit {
  _opened = false;
  _closeOnClickOutside: boolean = true;
  _closeOnClickBackdrop: boolean = true;
  _animate: boolean = true;
  _dock: boolean = true;
  constructor() { }
  public push = 'push';
  ngOnInit() {
  }

  cambiomenu(event: boolean) {
    console.log(event);
    this._opened = event;

  }
  cambio() {
    if (this._opened === false) {
      this._opened = true;
    }
  }
}
