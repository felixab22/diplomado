import { Component, OnInit } from '@angular/core';
import { EstudianteService } from '@services/services.index';

@Component({
  selector: 'app-alumno-pdf',
  templateUrl: './alumno-pdf.component.html',
  styleUrls: ['./alumno-pdf.component.scss']
})
export class AlumnoPdfComponent implements OnInit {
  listas: any[];
  totalfasepresencial = 0;
  totalfasepresencialno = 0;
  totalcredito = 0;

  constructor(
    private _EstudianteSrv: EstudianteService,
  ) { }

  ngOnInit() {
    const idestudiante = localStorage.getItem('klumno');
    const idsemestre = localStorage.getItem('kidsemestre');
    console.log(idestudiante, idsemestre);
    this.imprimir(idestudiante, idsemestre);

  }


  imprimir(idestu, idsemes) {
    this._EstudianteSrv.getListaAllDiplomadodeUnEstudent(idestu, idsemes).subscribe(res => {
      console.log(res);
      this.listas = res;
      this.totales();
    });
  }
  totales() {
    for (let item of this.listas) {
      this.totalfasepresencial = item.modulo.horafpresencial + this.totalfasepresencial;
      this.totalfasepresencialno = item.modulo.horafnopresencial + this.totalfasepresencialno;
      this.totalcredito = item.modulo.credfnopresencial + item.modulo.credfpresencial + this.totalcredito;
    }
  }
  createPDF() {
    var sTable = document.getElementById('imprimirpfgalumno').innerHTML;
    var style = "<style>";
    style = style + "div.container {font-family: Arial; width: 245mm; height: 210mm;}";
    // style = style + "div.container {font-family: Arial; width: 210mm; height: 245mm;}";      
    style = style + "table {width: 100%;font: 11px Arial;}";
    style = style + "table, th, td {border: solid 1px black; border-collapse: collapse;}";
    style = style + "th {text-align: center; font-size: 12px;font-weight: 400; font-family: Arial; height: 65px;}";
    style = style + "td {text-align: center; font-size: 12px;font-weight: 400; font-family: Arial;height: 65px;}";
    style = style + "td.nombre {text-align: left;}";
    // style = style + "padding: 2px 3px;text-align: center;}";

    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>ACTA DIPLOMADO</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }

}
