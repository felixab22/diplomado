import { Component, OnInit } from '@angular/core';
import { ActasService } from '@services/actas.service';
import { ActaModel, SemestreModal } from '@models/diplomado/diplomado.modal';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-semestre-alumno',
  templateUrl: './semestre-alumno.component.html',
  styleUrls: ['./semestre-alumno.component.scss']
})
export class SemestreAlumnoComponent implements OnInit {
  actas: any[];
  j = 1;
  pagina1 = 1;
  contA = 0;
  semestre: SemestreModal;
  allNotas: any[];
  Notasimprimir: any[];
  temporal: any;
  constructor(
    private _ActasSrv: ActasService,
    private _route: ActivatedRoute
  ) {
    this.allNotas = this._ActasSrv.Notas;
    this.Notasimprimir = new Array();
  }

  ngOnInit() {
    const id = this._route.snapshot.paramMap.get('idsemestre');
    this.semestre = JSON.parse(localStorage.getItem('cursonotas'));
    this.imprimirAlumnosInscritos(id);
    this.imprimirAllNota();
    this.incrustar();
  }
  imprimirAlumnosInscritos(id) {
    this._ActasSrv.getEstudentxSemestre(id).subscribe((res: any) => {
      this.allNotas = res.data;
      console.log(res);
    });
  }
  incrustar() {
    for (let item of this.Notasimprimir) {
      if (this.j < 10) {
        item.numero = '00' + this.j.toString();
      } else {
        item.numero = '0' + this.j.toString();
      }
      this.j = this.j + 1;
    }

  }


  createPDF() {
    var sTable = document.getElementById('imprimirpfg').innerHTML;
    var style = "<style>";
    // style = style + "div.container {font-family: Arial; width: 210mm; height: 245mm;}";
    // style = style + "h4 {text-align: center; padding:0; margin: 0; }";
    // style = style + "table {width: 100%; border-collapse: collapse;}";
    // style = style + "tr, td, th {border: 1px solid black;}";
    style = style + "div.container {font-family: Arial; width: 210mm; height: 245mm;}";
    style = style + ".header {display: grid; grid-template-columns: 15% 75%;}";
    style = style + "h3 , h5  {text-align: center; padding:0; margin: 0; }";
    style = style + "h6 {font-size: 10px;font-weight: 600; padding:0px; margin: 5px; }";
    style = style + ".sub-cabeza {display: grid; grid-template-columns: 18% 52% 10% 20%; padding:0;margin: 0; }";
    style = style + ".card {margin-top: 20px;}";
    style = style + ".logo img { width: 50%; height: 100%;}";
    style = style + "table {width: 100%;font: 11px Arial;}";
    style = style + "table, th, td {border: solid 1px black; border-collapse: collapse;}";
    style = style + "th {text-align: center; font-size: 10px;font-weight: 300; font-family: Arial;}";
    style = style + "td {text-align: center; font-size: 10px;font-weight: 300; font-family: Arial;height: 25px;}";
    style = style + "td.nombre {text-align: left;}";
    style = style + ".azul {color: blue;}";
    style = style + ".rojo {color: red;}";
    style = style + ".numeration { width: 30px;}";
    style = style + ".mod {width: 40px;}";


    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>LISTA ALUMNOS</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }
  // codigo para colocar la lista en un nombre las notas
  imprimirAllNota() {
    for (let dato of this.allNotas) {
      if (this.contA !== dato.codigo) {
        this.temporal = dato;
        this.contA = this.temporal.codigo;
      } else {
        if (dato.nmod1 !== 0) {
          this.temporal.nmod1 = dato.nmod1;
        }
        if (dato.nmod2 !== 0) {
          this.temporal.nmod2 = dato.nmod2;
        }
        if (dato.nmod3 !== 0) {
          this.temporal.nmod3 = dato.nmod3;
        }
        if (dato.nmod4 !== 0) {
          this.temporal.nmod4 = dato.nmod4;
        }
        if (dato.nmod5 !== 0) {
          this.temporal.nmod5 = dato.nmod5;
        }
        if (dato.nmod6 !== 0) {
          this.temporal.nmod6 = dato.nmod6;
        }
        if (dato.nmod7 !== 0) {
          this.temporal.nmod7 = dato.nmod7;
          this.Notasimprimir.push(this.temporal);
        }
      }
    }


  }

}
