import { Component, OnInit } from '@angular/core';
import { SemestreModal, ActaModel, ModulosModel } from '@models/diplomado/diplomado.modal';
import { ActasService, DiplomadoService, EstudianteService } from '@services/services.index';
import { Router } from '@angular/router';
declare var swal: any;
@Component({
  selector: 'app-actas',
  templateUrl: './actas.component.html',
  styleUrls: ['./actas.component.scss']
})
export class ActasComponent implements OnInit {
  detalleDiplo: SemestreModal;
  checkNum = true;
  term = '';
  p: number = 1;
  newActa: ActaModel;
  actas: ActaModel[];
  iddiplo = 0;
  buscaxDNI = null;
  datos: any;
  nombre = '';
  apellido = '';
  dni = 0;
  encontrado = true;
  codires: any;
  modulos: ModulosModel[];
  idestudiante: any;
  fechahoy: any;
  yainscrito: any;
  yaguardado: any;
  constructor(
    private _ActasSrv: ActasService,
    private _Router: Router,
    private _DiplomadoSrv: DiplomadoService,
    private _EstudianteSrv: EstudianteService,
  ) {
    this.yainscrito = 0;
    this.newActa = new ActaModel();
    this.newActa.fregistro = new Date();
    this.datos = [{ code: 0, data: null, message: '' }];
    this.fechahoy = new Date().toISOString().substr(0, 10);
  }

  ngOnInit() {
    this.detalleDiplo = JSON.parse(localStorage.getItem('curso'));
    this.buscarDiplomado();
    this.buscarModuloxDiplomado();
    this.imprimirAlumnosInscritos();
  }
  masterToggle() {
    const items: any = document.getElementsByName('acs');
    if (this.checkNum === true) {
      for (var i = 0; i < items.length; i++) {
        if (items[i].type == 'checkbox')
          items[i].checked = true;
      }
      this.checkNum = false;
    } else {
      // const items: any = document.getElementsByName('acs');
      for (var i = 0; i < items.length; i++) {
        if (items[i].type == 'checkbox')
          items[i].checked = false;
      }
      this.checkNum = true;
    }

  }
  BuscarAlumnoxDNI() {
    this.yainscrito = 0;
    this._EstudianteSrv.getEstudentxDNI(this.buscaxDNI).subscribe((res: any) => {
      if (res.code === 200) {
        this.codires = res.code;
        this.datos = res;
        this.nombre = this.datos.data.nombre;
        this.apellido = this.datos.data.apellido;
        this.dni = this.datos.data.dni;
        this.idestudiante = this.datos.data.idestudiante;
        this.encontrado = false;
      } else {
        this.encontrado = true;
        this.codires = res.code;
      }

    });
  }
  BuscarAlumnoxDNI2(dni) {
    this.yainscrito = 0;
    this._EstudianteSrv.getEstudentxDNI(dni).subscribe((res: any) => {
      if (res.code === 200) {
        this.codires = res.code;
        this.datos = res;
        this.nombre = this.datos.data.nombre;
        this.apellido = this.datos.data.apellido;
        this.dni = this.datos.data.dni;
        this.idestudiante = this.datos.data.idestudiante;
        this.encontrado = false;
      } else {
        this.encontrado = true;
        this.codires = res.code;
      }
    });
  }
  buscarDiplomado() {
    const listaDiploma = JSON.parse(localStorage.getItem('listaDiplomados'));
    for (let diploma of listaDiploma) {
      if (diploma.denomination === this.detalleDiplo.diplomado) {
        this.iddiplo = diploma.iddiplomado;
      }
    }
  }

  buscarModuloxDiplomado() {
    this._DiplomadoSrv.getListaModulosxIdDiplomado(this.iddiplo).subscribe(res => {
      this.modulos = res;
    });
  }
  imprimirAlumnosInscritos() {
    this._ActasSrv.getEstudentxSemestre(this.detalleDiplo.idsemestre).subscribe((res: any) => {
      if (res.code === 200) {
        this.actas = res.data;
      }
    })
  }

  SaveOrUpdateActas(acta) {
    this._ActasSrv.saveOrUpdateActa(acta).subscribe((res: any) => {
      this.yainscrito = res.code;
      this.encontrado = true;
      this.buscaxDNI = null;
    });
  }

  finalizarMatricula() {
    this.detalleDiplo.estado = 'INICIADO';
    this._DiplomadoSrv.saveOrUpdateSemestre(this.detalleDiplo).subscribe((res: any) => {
      if (res.code = 200) {
        swal('Bien!', 'Guardado!', 'success').then(() => {
          this._Router.navigate(['/Posgrado/Aperturar-diploma']);
        });
      } else {
        swal('Mal!', 'Los campos con * son obligatorios', 'warning');
      }
    });
  }
  GuardarAlumno() {
    for (let modulo of this.modulos) {
      this.newActa = new ActaModel();
      this.newActa.modulo = modulo.idmodulo;
      this.newActa.fregistro = this.fechahoy;
      this.newActa.semestre = this.detalleDiplo.idsemestre;
      this.newActa.estudiante = this.idestudiante;
      this.newActa.notamodulo = -1;
      this.SaveOrUpdateActas(this.newActa);
    }
    this.imprimirAlumnosInscritos();
  }

}
