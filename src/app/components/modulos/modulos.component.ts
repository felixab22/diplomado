import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DiplomadoService } from '@services/services.index';
import { DiplomadoModal, ModulosModel } from '@models/diplomado/diplomado.modal';

@Component({
  selector: 'app-modulos',
  templateUrl: './modulos.component.html',
  styleUrls: ['./modulos.component.scss']
})
export class ModulosComponent implements OnInit {
  diploma: DiplomadoModal;
  ModuloNew: ModulosModel
  modulos: ModulosModel[];
  constructor(
    private _route: ActivatedRoute,
    private _DiploadoSrv: DiplomadoService
  ) {
    this.ModuloNew = new ModulosModel();
  }

  ngOnInit() {
    const id = this._route.snapshot.paramMap.get('id');
    this.diploma = JSON.parse(localStorage.getItem('diploma'));
    this.imprimir();
  }
  imprimir() {
    this._DiploadoSrv.getListaModulosxIdDiplomado(this.diploma.iddiplomado).subscribe(res => {
      console.log(res.length);
      this.ModuloNew.nummodulo = res.length + 1;
      if (res.length === 0) {
        console.log('vasio');
        this.ModuloNew.nummodulo = res.length + 1;
      } else {
        this.modulos = res;
      }
    });
  }
  GuardarOeditar(basicModal) {
    this.ModuloNew.diplomado = this.diploma.iddiplomado;
    console.log(this.ModuloNew);
    this._DiploadoSrv.saveOrUpdateModulos(this.ModuloNew).subscribe(res => {
      this.imprimir();
      basicModal.hide();
    });
  }
  editarModule(module, basicModal) {
    this.ModuloNew = module;
    basicModal.show();
  }

}
