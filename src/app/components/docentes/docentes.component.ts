import { Component, OnInit } from '@angular/core';
import { DocenteService } from '@services/services.index';
import { DocenteModel } from '../../models/diplomado/diplomado.modal';
declare var swal: any;

@Component({
  selector: 'app-docentes',
  templateUrl: './docentes.component.html',
  styleUrls: ['./docentes.component.scss']
})
export class DocentesComponent implements OnInit {
  checkNum = true;
  term = '';
  p: number = 1;
  NewDocente: DocenteModel;
  docentes: DocenteModel[];
  constructor(
    private _DocenteSrv: DocenteService,
  ) {
    this.NewDocente = new DocenteModel();
  }
  ngOnInit() {
    this.listarDocente();
  }
  listarDocente() {
    this._DocenteSrv.getListaDocenteAll().subscribe(res => {
      this.docentes = res;
    });
  }
  masterToggle() {
    const items: any = document.getElementsByName('acs');
    if (this.checkNum === true) {
      for (var i = 0; i < items.length; i++) {
        if (items[i].type == 'checkbox')
          items[i].checked = true;
      }
      this.checkNum = false;
    } else {
      // const items: any = document.getElementsByName('acs');
      for (var i = 0; i < items.length; i++) {
        if (items[i].type == 'checkbox')
          items[i].checked = false;
      }
      this.checkNum = true;
    }
  }
  GuardarOeditar(basicModal) {
    this._DocenteSrv.saveOrUpdateDocente(this.NewDocente).subscribe((res: any) => {
      console.log(res);
      if (res.code === 200) {
        basicModal.hide();
        swal('Bien!', 'Guardado!', 'success').then(() => {
          this.NewDocente = new DocenteModel();
          this.listarDocente();
        });
      } else {
        swal('Mal!', 'Los campos con * son obligatorios', 'warning');
      }
    });
  }

}
