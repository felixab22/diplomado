import { Component, OnInit } from '@angular/core';
import { DiplomadoService, ActasService } from '@services/services.index';
@Component({
  selector: 'app-bienvenido',
  templateUrl: './bienvenido.component.html',
  styleUrls: ['./bienvenido.component.scss']
})
export class BienvenidoComponent implements OnInit {

  diplomados: any;
  allNotas: any[];
  temporal: any;
  contA = 0;
  Notasimprimir: Array<any>;
  ultimo: any[];
  constructor(
    private _DiplomateSrv: DiplomadoService,
    // private _ActasSrv: ActasService,
  ) {
    this.Notasimprimir = new Array<any>();
  }

  ngOnInit() {
    this.imprimirDiplomado();
    // this.imprimirAllNota();
  }
  imprimirDiplomado() {
    this._DiplomateSrv.getListaDiplomado().subscribe(res => {
      this.diplomados = res;
      localStorage.setItem('listaDiplomados', JSON.stringify(this.diplomados));
    });
  }

  // imprimirAllNota() {
  //   for (let dato of this.allNotas) {
  //     if (this.contA !== dato.codigo) {
  //       this.temporal = dato;
  //       this.contA = this.temporal.codigo;
  //     } else {
  //       if (dato.nmod1 !== 0) {
  //         this.temporal.nmod1 = dato.nmod1;
  //         this.Notasimprimir.push(this.temporal);
  //       }
  //       if (dato.nmod2 !== 0) {
  //         this.temporal.nmod2 = dato.nmod2;
  //       }
  //       if (dato.nmod3 !== 0) {
  //         this.temporal.nmod3 = dato.nmod3;
  //       }
  //       if (dato.nmod4 !== 0) {
  //         this.temporal.nmod4 = dato.nmod4;
  //       }
  //       if (dato.nmod5 !== 0) {
  //         this.temporal.nmod5 = dato.nmod5;
  //       }
  //       if (dato.nmod6 !== 0 && dato.nmod6 !== undefined) {
  //         this.temporal.nmod6 = dato.nmod6;
  //       }
  //       if (dato.nmod7 !== 0 && dato.nmod7 !== undefined) {
  //         this.temporal.nmod7 = dato.nmod7;
  //       }
  //       if (dato.nmod8 !== 0 && dato.nmod8 !== undefined) {
  //         this.temporal.nmod8 = dato.nmod8;
  //       }
  //       if (dato.nmod9 !== 0 && dato.nmod9 !== undefined) {
  //         this.temporal.nmod9 = dato.nmod9;
  //       }

  //     }

  //   }
  //   console.log(this.Notasimprimir);

  // }
}
