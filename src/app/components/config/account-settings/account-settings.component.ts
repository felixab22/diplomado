import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {
  sincheck = '';

  constructor(
    @Inject(DOCUMENT) private _document
    // public _ajustes: SettingsService
  ) { }

  ngOnInit() {
    this.colocarCheck();
  }
  cambiarColor(theme: string) {
    this.sincheck = theme;
    console.log(theme);
    let clasee = `navbar navbar-expand-lg navbar-dark ${theme}`;
    this._document.getElementById('temacolor').setAttribute('SideClass', clasee);

    // this.aplicarcheck(link);
    //  this._ajustes.aplicarTema(theme);
  }
  aplicarcheck(link: any) {
    let selectores: any = document.getElementsByClassName('selector');
    for (let ref of selectores) {
      ref.classList.remove('working')
    }
    link.classList.add('working');
  }
  colocarCheck() {
    let selectores: any = document.getElementsByClassName('selector');
    //  let tema = this._ajustes.ajustes.tema;

    //  for(let ref of selectores) {
    //    if(ref.getAttribute('data-theme') === tema){
    //      ref.classList.add('working');
    //      break;
    //    }
    //  }
  }

}


