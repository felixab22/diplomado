import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numero'
})
export class NumeroPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    switch (value) {
      case -1:
        return 'SIN NOTA';
      case 0:
        return 'CERO';
      case 1:
        return 'UNO';
      case 2:
        return 'DOS';
      case 3:
        return 'TRES';
      case 4:
        return 'CUATRO';
      case 5:
        return 'CINCO';
      case 6:
        return 'SEIS';
      case 7:
        return 'SIETE';
      case 8:
        return 'OCHO';
      case 9:
        return 'NUEVO';
      case 10:
        return 'DIEZ';
      case 11:
        return 'ONCE';
      case 12:
        return 'DOCE';
      case 13:
        return 'TRECE';
      case 14:
        return 'CATORCE';
      case 15:
        return 'QUINCE';
      case 16:
        return 'DIECISÉIS';
      case 17:
        return 'DIECISIETE';
      case 18:
        return 'DIECIOCHO';
      case 19:
        return 'DIECINUEVE';
      case 20:
        return 'VEINTE';
      default:
        return 'NO VALIDO'
    }
  }

}
