/* cSpell:disable */
import { Routes, RouterModule } from '@angular/router';
import { ComponentsComponent } from './components/components.component';
import { BienvenidoComponent } from './components/config/bienvenido/bienvenido.component';
import { LoginComponent } from './shared/login/login.component';
import { NopageComponent } from './shared/nopagecomponent/nopagecomponent.component';
import { AccountSettingsComponent } from '@components/config/account-settings/account-settings.component';
import { DiplomateListComponent } from '@components/diploma/diplomate-list/diplomate-list.component';
import { AperturaDiplomaComponent } from '@components/diploma/apertura-diploma/apertura-diploma.component';
import { AlumnoListaComponent } from '@components/alumno-lista/alumno-lista.component';
import { ModulosComponent } from '@components/modulos/modulos.component';
import { ActasComponent } from '@components/actas/actas.component';
import { NotasComponent } from '@components/notas/notas.component';
import { PdfComponent } from '@components/pdf/pdf.component';
import { DocentesComponent } from '@components/docentes/docentes.component';
import { SemestreAlumnoComponent } from '@components/semestre-alumno/semestre-alumno.component';
import { AlumnoPdfComponent } from '@components/alumno-pdf/alumno-pdf.component';

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: '', component: LoginComponent },
    {
        path: 'Posgrado',
        component: ComponentsComponent,
        // canActivate: [AuthGuard],
        children:
            [
                { path: 'bienvenido', component: BienvenidoComponent },
                { path: 'Diplomado', component: DiplomateListComponent },
                { path: 'Actas', component: ActasComponent },
                { path: 'pdf', component: PdfComponent },
                { path: 'Notas', component: NotasComponent },
                { path: 'Modulos/:id', component: ModulosComponent },
                { path: 'Alumnos-lista', component: AlumnoListaComponent },
                { path: 'Docente', component: DocentesComponent },
                { path: 'Alumnopdf', component: AlumnoPdfComponent },
                { path: 'Aperturar-diploma', component: AperturaDiplomaComponent },
                { path: 'Semestre-alumno/:idsemestre', component: SemestreAlumnoComponent },
                { path: 'account-settings', component: AccountSettingsComponent },
                { path: '**', component: NopageComponent }
            ]
    },
];

export const APP_ROUTES = RouterModule.forRoot(appRoutes, { useHash: true });
